import React from 'react';
import { GoogleMap, useLoadScript, Polygon } from '@react-google-maps/api';
import Button from '@mui/material/Button';
import { Tab } from "./components/Tab/Tab"

// const containerStyle = {
//   width: '100vw',
//   height: '100vh',
// };

// const center = {
//   lat: 48.8568042,
//   lng: 2.3641117,
// };

// const options = {
//   styles: mapStyles,
// };

// const paths = [
//   { lng: 2.3327524136, lat: 48.8838863538 },
//   { lng: 2.3323825558, lat: 48.8848840041 },
//   { lng: 2.3300324693, lat: 48.887483336 },
//   { lng: 2.3319177391, lat: 48.88873384 },
//   { lng: 2.3322652149, lat: 48.8892101928 },
//   { lng: 2.3329068616, lat: 48.8887702678 },
//   { lng: 2.3330615407, lat: 48.8885416667 },
//   { lng: 2.3336277274, lat: 48.8881342819 },
//   { lng: 2.3332229747, lat: 48.8876439859 },
//   { lng: 2.3339367899, lat: 48.8871980325 },
//   { lng: 2.3345319902, lat: 48.8866298287 },
//   { lng: 2.3349825491, lat: 48.8857627926 },
//   { lng: 2.3347383523, lat: 48.8858393816 },
//   { lng: 2.3327524136, lat: 48.8838863538 }


// ]

// const coords = [
//   [
//     [
//       2.3327524136,
//       48.8838863538
//     ],
//     [
//       2.3323825558,
//       48.8848840041
//     ],
//     [
//       2.3300324693,
//       48.887483336
//     ],
//     [
//       2.3319177391,
//       48.88873384
//     ],
//     [
//       2.3322652149,
//       48.8892101928
//     ],
//     [
//       2.3329068616,
//       48.8887702678
//     ],
//     [
//       2.3330615407,
//       48.8885416667
//     ],
//     [
//       2.3336277274,
//       48.8881342819
//     ],
//     [
//       2.3332229747,
//       48.8876439859
//     ],
//     [
//       2.3339367899,
//       48.8871980325
//     ],
//     [
//       2.3345319902,
//       48.8866298287
//     ],
//     [
//       2.3349825491,
//       48.8857627926
//     ],
//     [
//       2.3347383523,
//       48.8858393816
//     ],
//     [
//       2.3327524136,
//       48.8838863538
//     ]
//   ]
// ]

function App() {

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
  });

  if (loadError) return 'Error loading maps';
  if (!isLoaded) return 'Loading maps';

  return (
    <div className="App">
      <Tab />
    </div>
  );
}

export default App;