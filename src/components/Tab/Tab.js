import React from "react"
import { GoogleMap, useLoadScript, Polygon } from '@react-google-maps/api';
import { mapStyles } from '../mapsStyles';
import Button from '@mui/material/Button';
import * as axios from "axios";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { getLRColorFirstRound, getRNColorFirstRound, getABSTColorFirstRound, getLFITColorFirstRound, getLREMColorFirstRound, getPSColorFirstRound } from "./colorFirstRound"
import { getLREMColorSecondRound, getRNColorSecondRound } from "./colorSecondRound";

const LR = "LR"
const RN = "RN"
const PS = "PS"
const LREM = "LREM"
const LFI = "LFI"
const ABST = "Abstentionniste"

const containerStyle = {
    width: '100vw',
    height: '100vh',
};

const center = {
    lat: 48.8568042,
    lng: 2.3641117,
};

const options = {
    styles: mapStyles,
};

function getColor(percent, target, round) {
    if (round === 1) {
        switch (target) {
            case LR:
                return getLRColorFirstRound(percent)
            case RN:
                return getRNColorFirstRound(percent)
            case ABST:
                return getABSTColorFirstRound(percent)
            case LREM:
                return getLREMColorFirstRound(percent)
            case PS:
                return getPSColorFirstRound(percent)
            case LFI:
                return getLFITColorFirstRound(percent)
            default:
                console.log(`[getColor] Target is not supported '${target}' for the round '${round}'`)
        }
    } else if (round === 2) {
        switch (target) {
            case RN:
                return getRNColorSecondRound(percent)
            case LREM:
                return getLREMColorSecondRound(percent)
            default:
                console.log(`[getColor] Target is not supported '${target}' for the round '${round}'`)
        }
    }
}

function calculatePercentByCandidat(candidat, electionOffice) {
    return (candidat / electionOffice.nb_votant).toFixed(2) * 100
}

function getPercentByTarget(electionOffice, target) {
    switch (target) {
        case LR:
            return calculatePercentByCandidat(electionOffice.fillon_francois, electionOffice)
        case RN:
            return calculatePercentByCandidat(electionOffice.le_pen_marine, electionOffice)
        case PS:
            return calculatePercentByCandidat(electionOffice.hamon_benoit, electionOffice)
        case LREM:
            return calculatePercentByCandidat(electionOffice.macron_emmanuel, electionOffice)
        case LFI:
            return calculatePercentByCandidat(electionOffice.melenchon_jean_luc, electionOffice)
        case ABST:
            return (1 - (electionOffice.nb_votant / electionOffice.nb_inscr)).toFixed(2) * 100
        default:
            console.log("[getPercentByTarget] Target is not supported", target)
    }
}

function createPolygone(electionOffice, target, idx, round) {
    const percent = getPercentByTarget(electionOffice, target)
    return (<Polygon
        path={electionOffice.coordinates}
        key={target + idx}
        options={{
            fillColor: getColor(percent, target, round),
            fillOpacity: 0.35,
            strokeColor: getColor(percent, target, round),
            strokeOpacity: 0.8,
            strokeWeight: 2,
        }}
        onClick={() => {
            alert(`\nnb_inscrit: ${electionOffice.nb_inscr}\nnb_votant: ${electionOffice.nb_votant}\nLR: ${electionOffice.fillon_francois}\nRN: ${electionOffice.le_pen_marine}\n\nabst_percent: ${((1 - (electionOffice.nb_votant / electionOffice.nb_inscr)).toFixed(2) * 100)}%\nlr_percent: ${calculatePercentByCandidat(electionOffice.fillon_francois, electionOffice)}%\nrn_percent: ${calculatePercentByCandidat(electionOffice.le_pen_marine, electionOffice)}%\nlfi_percent: ${calculatePercentByCandidat(electionOffice.melenchon_jean_luc, electionOffice)}%\nlrem_percent: ${calculatePercentByCandidat(electionOffice.macron_emmanuel, electionOffice)}%\nps_percent: ${calculatePercentByCandidat(electionOffice.hamon_benoit, electionOffice)}%`)
        }}
    />)
}

async function getAllElectionOffices(round, offset = 0, limit = 100, offices = []) {
    try {
        const result = await axios.default.get(`${process.env.REACT_APP_BACK_ENDURL}/elections/office?offset=${offset}&limit=${limit}&round=${round}`)

        const total = result.data.pagination.total

        offices.push(...result.data.records)
        if (offset < total) {
            offset += limit
            return getAllElectionOffices(round, offset, limit, offices)
        }
        return offices
    } catch (err) {
        console.log("err", err)
    }
    return offices
}

export class Tab extends React.Component {
    constructor(props) {
        super()

        this.state = {
            politicsGroup: RN,
            politicGroupList: [
                [LR, RN, ABST, PS, LREM, LFI],
                [RN, LREM]
            ],
            round: 1,
            firstRoundOffices: [],
            secondRoundOffices: [],
            electionsOffices: []
        }
    }

    handlePoliticGroupChange = (event) => {
        this.setState({ politicsGroup: event.target.value })
    };

    handleRoundChange = async (event) => {
        this.setState({ round: event.target.value, electionsOffices: event.target.value === 1 ? this.state.firstRoundOffices : this.state.secondRoundOffices })
    };

    async componentDidMount() {
        const firstRoundOffices = await getAllElectionOffices(1)
        this.setState({ electionsOffices: firstRoundOffices, firstRoundOffices: firstRoundOffices, secondRoundOffices: await getAllElectionOffices(2) })
    }

    render() {
        return (
            <div>
                <FormControl style={{ margin: 7, width: "20%" }}>
                    <InputLabel id="round-select-label">Tour de l'élection</InputLabel>
                    <Select
                        labelId="round-select-label"
                        id="round-select"
                        value={this.state.round}
                        label="Tour de l'élection"
                        onChange={this.handleRoundChange}
                    >
                        <MenuItem value={1}>Premier tour</MenuItem>
                        <MenuItem value={2}>Second tour</MenuItem>
                    </Select>
                </FormControl>

                <FormControl style={{ margin: 7, width: "20%" }}>
                    <InputLabel id="political-group-select-label">Parti politique</InputLabel>
                    <Select
                        labelId="political-group-select-label"
                        id="political-group-select"
                        value={this.state.politicsGroup}
                        label="Parti politique"
                        onChange={this.handlePoliticGroupChange}
                    >
                        {
                            this.state.politicGroupList[this.state.round - 1].map((value, idx) => {
                                return (<MenuItem key={"political-group-" + idx} value={value}>{value}</MenuItem>)
                            })
                        }
                    </Select>
                </FormControl>

                <GoogleMap
                    mapContainerStyle={containerStyle}
                    center={center}
                    zoom={13}
                    options={options}
                >
                    {
                        this.state.electionsOffices.map((electionOffice, idx) => {
                            return createPolygone(electionOffice, this.state.politicsGroup, `${this.state.politicsGroup}-${idx}`, this.state.round)
                        })
                    }
                </GoogleMap>
            </div>
        );
    }
}