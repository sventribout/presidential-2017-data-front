export function getLRColorFirstRound(percent) {
    if (percent > 50) {
        return "#E31F1F"
    } else if (percent >= 30) {
        return "#E3721F"
    }
    else if (percent >= 20) {
        return "#E3991F"
    }
    else if (percent >= 15) {
        return "#E3AE1F"
    }
    else if (percent >= 10) {
        return "#E3DA1F"
    }
}

export function getRNColorFirstRound(percent) {
    if (percent > 10) {
        return "#E31F1F"
    } else if (percent >= 7) {
        return "#E3721F"
    }
    else if (percent >= 5) {
        return "#E3991F"
    }
    else if (percent >= 3) {
        return "#E3AE1F"
    }
}

export function getABSTColorFirstRound(percent) {
    if (percent > 30) {
        return "#E31F1F"
    } else if (percent >= 20) {
        return "#E3721F"
    }
    else if (percent >= 15) {
        return "#E3991F"
    }
    else if (percent >= 10) {
        return "#E3AE1F"
    }
    else if (percent >= 7) {
        return "#E3DA1F"
    }
}

export function getPSColorFirstRound(percent) {
    if (percent > 20) {
        return "#E31F1F"
    } else if (percent >= 15) {
        return "#E3721F"
    }
    else if (percent >= 10) {
        return "#E3991F"
    }
    else if (percent >= 7) {
        return "#E3AE1F"
    }
    else if (percent >= 5) {
        return "#E3DA1F"
    }
}

export function getLREMColorFirstRound(percent) {
    if (percent > 50) {
        return "#E31F1F"
    } else if (percent >= 40) {
        return "#E3721F"
    }
    else if (percent >= 30) {
        return "#E3991F"
    }
    else if (percent >= 20) {
        return "#E3AE1F"
    }
    else if (percent >= 10) {
        return "#E3DA1F"
    }
}


export function getLFITColorFirstRound(percent) {
    if (percent > 50) {
        return "#E31F1F"
    } else if (percent >= 30) {
        return "#E3721F"
    }
    else if (percent >= 20) {
        return "#E3991F"
    }
    else if (percent >= 10) {
        return "#E3AE1F"
    }
    else if (percent >= 7) {
        return "#E3DA1F"
    }
}
