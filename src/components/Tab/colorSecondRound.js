export function getRNColorSecondRound(percent) {
    if (percent > 20) {
        return "#E31F1F"
    } else if (percent >= 15) {
        return "#E3721F"
    }
    else if (percent >= 10) {
        return "#E3991F"
    }
    else if (percent >= 7) {
        return "#E3AE1F"
    }
    else if (percent >= 5) {
        return "#E3DA1F"
    }
}


export function getLREMColorSecondRound(percent) {
    if (percent > 90) {
        return "#E31F1F"
    } else if (percent >= 85) {
        return "#E3721F"
    }
    else if (percent >= 80) {
        return "#E3991F"
    }
    else if (percent >= 70) {
        return "#E3AE1F"
    }
    else if (percent >= 50) {
        return "#E3DA1F"
    }
}
